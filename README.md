# Preparation

on lxplus8.cern.ch

```
export CLUSTER_NAME="<cluster name>"

export KUBECONFIG=$HOME/cern/openstack/$CLUSTER_NAME/config

kubectl get nodes

kubectl label nodes <node>-# winccx.node.role=true
```
Detailed informaton on [ creating a K8s cluster ](https://gitlab.cern.ch/cmsos/kube/-/wikis/Creating%20a%20Kubernetes%20cluster%20in%20OpenStack) on IT services

# Helm commands
```
helm repo add winccx-devel https://gitlab.cern.ch/api/v4/projects/183203/packages/helm/devel

helm repo update

helm search repo --devel

helm uninstall winccx

```
### Installation on general purpose cluster

```
helm install winccx winccx-devel/cmsos-winccx-helm --devel
```

### Installation in CMS cluster

```
helm install --set zone.name=development --set zone.imagepullsecrets=false --set zone.createnamespace=false winccx winccx-devel/cmsos-winccx-helm --devel
```

# kubectl commands

kubectl get pods -n winccx

kubectl get services -n winccx -o wide

kubectl exec -ti <podid> -n winccx -- bash

## Running on daq3val K8s cluster

### Prepare cluster

```
kubectl config use-context kubernetes-admin@kubernetes
kubectl get nodes
kubectl label nodes d3vrubu-c2e34-06-01 winccx.node.role=true
kubectl label nodes d3vrubu-c2e34-08-01 winccx.node.role=true
kubectl label nodes d3vrubu-c2e34-10-01 winccx.node.role=true
```

### install winccx

```
helm repo add winccx-devel https://gitlab.cern.ch/api/v4/projects/177090/packages/helm/devel
helm install winccx -devel/cmsos-notice-helm --devel
helm install winccx -devel/cmsos-notice-helm --devel --set zone.application.source.replicas=3
```

### Bash directly into pod and CURL 
```
kubectl exec -ti source-??????  -n winccx -- bash
export K8S_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
cat >> event.json
curl -k -v -X POST -H "Authorization: Bearer $K8S_TOKEN" -H "Content-Type: application/json" https://10.254.0.1:443/api/v1/namespaces/winccx/winccx -d@event.json
```

### Watching for winccx
```
kubectl get winccx -n winccx  --watch
kubectl get winccx --field-selector type=Normal -n winccx  --watch  -o=jsonpath='{.metadata.name} {.eventTime} {.involvedObject.name} {.reportingInstance} {.action} {.reason}{"\n"}'
kubectl get winccx -n winccx  --watch  -o=jsonpath='{.metadata.name} {.eventTime} {.involvedObject.name} {.reportingInstance} {.action} {.reason}{"\n"}' --selector type=winccx.fsm.state
```

### Adding worksuite project as a submodule of k8s/winccx
```
$ cd k8sbox/winccx
$ git submodule add https://gitlab.cern.ch/cmsos/worksuite
$ cd worksuite
$ git checkout <branch, tag or commit>
$ cd k8sbox/winccx
$ git add .gitmodules worksuite
$ git commit -m "Added submodule"
$ git push
```

### Modifying files inside submodule directory
```
$ cd k8sbox/winccx/worksuite
$ git add <file name>
$ git commit -m "message"
$ git push
$ cd k8sbox/winccx
$ git add worksuite
$ git commit -m "Updated submodule"
$ git push
```
